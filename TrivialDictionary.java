import java.util.Random;

class TrivialDictionary {

	public boolean sizeKnown = false;

	public static String wordAt(Integer index) {
		// implementation
		// assume the dictionary is already alphabetically sorted
	}

	// public constructor - when creating new TrivialDictionary, the boolean field sizeKnown is set to false - could do this straight with a (null) Integer object??
    // Since this is internal state, you could do either way -- as long as it is appropriately
    // documented. Anyway, personally I would prefer your current version with an explicit boolean :)

	public static boolean isInDictionary(String word) {
		
		// find size of dictionary -- one-off cost
		if(!sizeKnown) {

			Integer bound = guessUpperBound();
            // `size` is a local variable of this block and never escapes it?
			Integer size = findSize(bound);
			sizeKnown = true;

		}

		// binary search to find word 
		Integer head = 0, tail = size - 1;

		while (head != tail) {
			String mid = wordAt((tail-head)/2);
			int comparison = mid.compareToIgnoreCase(word);

			if(comparison == 0)
				return true;
			else if(comparison < 0)
					tail = (tail-head) / 2;
			else
				head = (tail-head) / 2;
		}

		return false;

		// for extra points - idea :
		// Implement a type of cache when searching for a word? Whenever we search for a new word,
		// we first check the cache (an ArrayList and use contains() method), and if it's not there, 
		// we resort to using wordAt() and add it to the cache (add() method).
		// The cache should have a set size and if it reaches its maximum,we replace the oldest entries
		// with new ones. Have a counter that keeps track of that (pointing at the oldest element in the cache).
        // A cache would make sense if you knew that queries have some relationships (e.g. repeated
        // queries for some words). Otherwise, why not put all the words in the cache to start
        // with?...
		
	}

	public static Integer guessUpperBound() {

        // Why would you generate a random number rather than providing a guess yourself? You risk
        // that the RNG will return something like 1 that you know is probably not appropriate. For
        // instance, you could either pick something like, say, 10,000 words expected (based on your
        // previous experience with dictionaries or something), or directly Integer.MAX_VALUE (even 
        // if it were 2^32 -- which is not --, it would need at most 32 iterations of halving to reach
        // the appropriate range, which is not too bad).
		// generate random number;
		// compare to size;
		// if guess >= actual size, return the guessed number
		// if guess < actual size, keep generating random number larger than its predecessor

	}

	public static Integer findSize(Integer upperBound) {

		// binary search to find size of dictionary
        // Hint: Do you need the actual size, or is an approximation enough? (See my previous
        // comment too)

		Integer head = 0, tail = upperBound;

		while (head != tail) {
			Integer mid = (tail-head) / 2;

			if(mid == actualSize)
                // Doesn't this method return an `Integer`?
				return true;
			else if(mid < 0)
					tail = (tail-head) / 2;
			else
				head = (tail-head) / 2;
		}
        // Missing return statement...

	}
}
